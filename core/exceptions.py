# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring
from types import TracebackType
from typing import Literal, Optional, Type


class PyAsmError(Exception):
    """
    Base Exception class
    """


class UndefinedInstructionError(PyAsmError):
    """
    Raised when there is no such instruction
    """


class UndefinedLOCError(PyAsmError):
    """
    Raised when translator can't classify line as Label or Instruction
    """


class UnexpectedOperandError(PyAsmError):
    """
    Raised when translator couldn't parse operand
    """


class UnexpectedArgumentsError(PyAsmError):
    """
    Raised when instruction argument type doesn't match expected
    """


class NotEnoughOperandsError(PyAsmError):
    """
    Raised when instruction needs more operands
    """


class UnexpectedDataValueError(PyAsmError):
    """
    Raised when data value is neither number nor string
    """


class TextSectionNotFoundError(PyAsmError):
    """
    Raised when
        section .text
    in assembly code not found
    """


class DataNotFoundError(PyAsmError):
    """
    Raised when variable in data section not found
    """


class IncorrectDataTypeError(PyAsmError):
    """
    Raised when needed data has another type
    """


class OperandIsNotWriteableError(PyAsmError):
    """
    Raised when trying to write in constant
    """


class NoSuchLabelError(PyAsmError):
    """
    Raised when translator can not find label
    """


class ProgramExitError(PyAsmError):
    """
    End of program
    """


class NumberOutOfRangeError(PyAsmError):
    """
    Raised when number in code is out of range
    """


class OperandMustBeCharNotStringError(PyAsmError):
    """
    Raised when string length is greater than one
    """


class NotEnoughMemoryError(PyAsmError):
    """
    Raised when not enough memory to load program data
    """


class ALUZeroDivisionErrorError(PyAsmError):
    """
    Raised when ALU tries to divide by zero
    """


class CatchPyAsmException:
    """
    Context manager that handles unexpected exceptions
    """

    def __init__(self):
        self.exception: Optional[PyAsmError] = None

    def __enter__(self) -> "CatchPyAsmException":
        return self

    def __exit__(
        self, exc_type: Optional[Type[BaseException]], exc_val: Optional[BaseException], exc_tb: Optional[TracebackType]
    ) -> Optional[Literal[True]]:
        if exc_val and isinstance(exc_val, PyAsmError):
            self.exception = exc_val
        elif exc_val and exc_type:
            tb_info: str = exc_tb and str(exc_tb.tb_frame) or "<None>"
            self.exception = PyAsmError(
                f"Unhandled exception.\n" f"\t{exc_type.__name__}: {exc_val!s}\n" f"\tInformation: {tb_info}"
            )

        return True
