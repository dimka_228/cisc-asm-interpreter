# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring

from core.machine.config import NULL_TERM


class IOController:

    """
    Input-Output Controller class
    """

    def __init__(self) -> None:
        self.input_buffer: list[list[int]] = []
        self.output_buffer: list[int] = []

    def putc(self, char: int) -> None:
        """
        Put symbol into buffer
        """
        self.output_buffer.append(char)

    def getc(self) -> int:
        """
        Get symbol from buffer
        """
        try:
            return self.input_buffer.pop(0)[1]
        except IndexError:
            return NULL_TERM

    def __str__(self) -> str:
        in_log: dict[str, int] = {
            "len": len(self.input_buffer),
            "last": chr(self.input_buffer[-1][1]) if len(self.input_buffer) > 0 else None,
        }
        out_log: dict[str, int] = {
            "len": len(self.output_buffer),
            "last": chr(self.output_buffer[-1]) if len(self.output_buffer) > 0 else None,
        }
        return f"input_buf: {in_log!s}, output_buf: {out_log!s}"
