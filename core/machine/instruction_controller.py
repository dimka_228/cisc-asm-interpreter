# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring
import operator
from typing import Callable, Iterable, Iterator, Optional

from core.exceptions import OperandIsNotWriteableError, ProgramExitError
from core.machine.alu import ALU, Flag
from core.machine.clock import ClockGenerator
from core.machine.config import FLAG_SAVE_ADDRESS, NULL_TERM, REG_SAVE_ADDRESS
from core.machine.int_controller import InterruptController
from core.machine.memory_controller import MemoryController
from core.machine.register_controller import RegisterController
from core.model import Address, Destination, IndirectAddress, Instruction, Label, Operand, Register, Source


class InstructionController:
    """
    Instruction Controller class
        - current       -- the current executing instruction
        - current_sub   -- the current executing sub instruction
    """

    __reduce_ops__ = {
        "add",
        "sub",
        "mul",
        "div",
        "mod",
        "xor",
        "and",
        "or",
    }

    def __init__(
        self,
        clock: ClockGenerator,
        alu: ALU,
        memory: MemoryController,
        registers: RegisterController,
        interrupt: InterruptController,
    ) -> None:
        self.current: Optional[Instruction] = None
        self.current_sub: Optional[Instruction] = None

        self.clock = clock
        self.alu = alu
        self.memory = memory
        self.registers = registers
        self.interrupt = interrupt

    def get_operand_value(self, operand: Operand) -> int:
        """
        Get operand value.
            - Indirect Address: shift address and work like with direct
            - Direct Address: get value with MemoryController
            - Register: get value with RegisterController
            - Label: get index
            - Constant: just get value
        """
        if isinstance(operand, IndirectAddress):
            offset: int = self.get_operand_value(operand.offset)

            self.registers.set_reg("AR", operand.value)
            self.registers.set_reg("DR", offset)
            self._exec_alu(operator.add, Register(name="AR"), Register(name="DR"))
            self.registers.set_reg("DR", self.memory.get(Address(value=self.registers.get_reg("AR"))))
            return self.registers.get_reg("DR")
        if isinstance(operand, Address):
            self.registers.set_reg("AR", operand.value)
            self.registers.set_reg("DR", self.memory.get(Address(value=self.registers.get_reg("AR"))))
            return self.registers.get_reg("DR")
        if isinstance(operand, Register):
            return self.registers.get(operand)
        return operand.value

    def set_operand_value(self, operand: Operand, value: int) -> None:
        """
        Set operand value.
            - Indirect Address: shift address and work like with direct
            - Direct Address: set value with MemoryController
            - Register: set value with RegisterController
            - Other: throw OperandIsNotWriteable
        """
        if isinstance(operand, IndirectAddress):
            offset: int = self.get_operand_value(operand.offset)
            self.registers.set_reg("AR", operand.value)
            self.registers.set_reg("DR", offset)
            self._exec_alu(operator.add, Register(name="AR"), Register(name="DR"))
            self.registers.set_reg("DR", value)
            self.memory.set(Address(value=self.registers.get_reg("AR")), self.registers.get_reg("DR"))

        elif isinstance(operand, Address):
            self.registers.set_reg("DR", value)
            self.registers.set_reg("AR", operand.value)
            self.memory.set(Address(value=self.registers.get_reg("AR")), self.registers.get_reg("DR"))
        elif isinstance(operand, Register):
            self.registers.set(operand, value)
        else:
            raise OperandIsNotWriteableError(operand.value)

    def save_state(self) -> None:
        mask: int = self.alu.get_flags()
        self.set_operand_value(Address(FLAG_SAVE_ADDRESS, ""), mask)
        self.set_operand_value(Address(REG_SAVE_ADDRESS, ""), self.registers.get_instruction_pointer())

    def load_state(self) -> None:
        self.alu.set_flags(self.get_operand_value(Address(FLAG_SAVE_ADDRESS, "")))
        self._jump_to(Label("", self.get_operand_value(Address(REG_SAVE_ADDRESS, ""))))

    def _jump_to(self, label: Label) -> None:
        """
        Set instruction pointer to label value
        """
        self.registers.set_instruction_pointer(label.value - 1)

    def _reduce_op(self, reducer: Callable, dest: Destination, *operands: Source) -> Iterator:
        """
        Defines operation behavior applying reducer function to operands
            - Two operands.
                Apply reducer to them and save result into "dest"
            - Three and more operands.
                Apply reducer to "*operands" and save result into "dest"
        """
        operand: Source = operands[0]
        op1: int = self.get_operand_value(dest)
        op2: int = self.get_operand_value(operand)
        self.clock.tick()
        yield
        result = self.alu.operation(reducer, op1, op2)
        self.clock.tick()
        yield
        self.set_operand_value(dest, result)

    def _exec_alu(self, reducer: Callable, dest: Destination, *operands: Source) -> None:
        """
        execute alu instruction without affecting inst pointer
        """
        operand: Source = operands[0]
        op1: int = self.get_operand_value(dest)
        op2: int = self.get_operand_value(operand)
        self.clock.tick()
        result = self.alu.operation(reducer, op1, op2)
        self.clock.tick()
        self.set_operand_value(dest, result)

    def _jmp_if(self, label: Label, condition: bool) -> None:
        """
        Jump to label if condition is True
        """
        if condition:
            self._jump_to(label)

    def i_add(self, dest: Destination, *ops: Source) -> Iterator:
        """
        ADD dest, *ops

        Sum operands amd save into dest
            - ADD A, B
                A = A + B
            - ADD A, B, C, D
                A = ((B + C) + D)
        """
        yield from self._reduce_op(operator.add, dest, *ops)

    def i_sub(self, dest: Destination, *ops: Source) -> Iterator:
        """
        SUB dest, *ops

        Subtract operands amd save into dest.
        Same logic as add
        """
        yield from self._reduce_op(operator.sub, dest, *ops)

    def i_mul(self, dest: Destination, *ops: Source) -> Iterator:
        """
        MUL dest, *ops

        Multiply operands amd save into dest
        Same logic as add
        """
        yield from self._reduce_op(operator.mul, dest, *ops)

    def i_div(self, dest: Destination, *ops: Source) -> Iterator:
        """
        DIV dest, *ops

        Divide (floor) operands amd save into dest.
        Same logic as add

        Can raise ALUDZeroDivisionError
        """
        yield from self._reduce_op(operator.floordiv, dest, *ops)

    def i_mod(self, dest: Destination, *ops: Source) -> Iterator:
        """
        MOD dest, *ops

        Modulo divide operands amd save into dest.
        Same logic as add

        Can raise ALUDZeroDivisionError
        """
        yield from self._reduce_op(operator.mod, dest, *ops)

    def i_xor(self, dest: Destination, *ops: Source) -> Iterator:
        """
        XOR dest, *ops

        Apply logical XOR to operands amd save into dest
        Same logic as add
        """
        yield from self._reduce_op(operator.xor, dest, *ops)

    def i_and(self, dest: Destination, *ops: Source) -> Iterator:
        """
        AND dest, *ops

        Apply logical AND to operands amd save into dest
        Same logic as add
        """
        yield from self._reduce_op(operator.and_, dest, *ops)

    def i_or(self, dest: Destination, *ops: Source) -> Iterator:
        """
        OR dest, *ops

        Apply logical OR to operands amd save into dest
        Same logic as add
        """
        yield from self._reduce_op(operator.or_, dest, *ops)

    def i_dec(self, dest: Destination) -> Iterator:
        """
        DEC dest

        Decrement (-1) operand
        """
        value: int = self.get_operand_value(dest)
        self.clock.tick()
        yield
        self.set_operand_value(dest, value - 1)

    def i_inc(self, dest: Destination) -> Iterator:
        """
        INC dest

        Increment (+1) operand
        """
        value: int = self.get_operand_value(dest)
        self.clock.tick()
        yield
        self.set_operand_value(dest, value + 1)

    def i_iret(self) -> None:
        """
        IRET

        Return from interruption.
        """
        self.interrupt.set_interrupted(False)
        self.load_state()

    def i_ei(self) -> None:
        """
        EI

        Enable interruption
        """
        self.interrupt.set_enable(True)

    def i_di(self) -> None:
        """
        DI

        Disable interruption
        """
        self.interrupt.set_enable(False)

    def i_jmp(self, label: Label) -> None:
        """
        JMP label

        Jump to label without condition
        """
        self._jump_to(label)

    def i_je(self, label: Label) -> None:
        """
        JE label

        Jump to label if Z Flag is set (operands are equal)
        """
        self._jmp_if(label, self.alu.get_flag(Flag.Z))

    def i_jne(self, label: Label) -> None:
        """
        JE label

        Jump to label if Z Flag is not set (operands are not equal)
        """
        self._jmp_if(label, not self.alu.get_flag(Flag.Z))

    def i_jl(self, label: Label) -> None:
        """
        JL label

        Jump to label if N Flag is set (first < second)
        """
        self._jmp_if(label, self.alu.get_flag(Flag.N))

    def i_jg(self, label: Label) -> None:
        """
        JL label

        Jump to label if N Flag is not set (first > second)
        """
        self._jmp_if(label, not self.alu.get_flag(Flag.N))

    def i_jle(self, label: Label) -> None:
        """
        JLE label

        Jump to label if Z or N flag is set (first <= second)
        """
        self._jmp_if(label, (self.alu.get_flag(Flag.Z) or self.alu.get_flag(Flag.N)))

    def i_jge(self, label: Label) -> None:
        """
        JGE label

        Jump to label if Z or not N flag is set (first >= second)
        """
        self._jmp_if(label, (self.alu.get_flag(Flag.Z) or not self.alu.get_flag(Flag.N)))

    def i_mov(self, dest: Destination, src: Source) -> Iterator:
        """
        MOV dest, src

        Move value from src to dest

        If dest is #STDOUT or #STDERR then src value
        will be written in stdout or stderr respectively
        """
        value: int = self.get_operand_value(src)
        self.clock.tick()
        yield
        self.set_operand_value(dest, value)

    def i_movn(self, dest: Address, src: Source) -> Iterator:
        """
        MOVN dest, src

        Move number value from src to #STDOUT or #STDERR
        """
        value: int = self.get_operand_value(src)
        self.clock.tick()
        yield
        for digit in str(value):
            self.set_operand_value(dest, ord(digit))
            self.clock.tick()
            yield

    def i_ldn(self, dest: Address, src: Source) -> Iterator:
        """
        LDN dest, src

        Get number value from #STDIN and write into dest
        """
        result: str = ""
        while (digit := self.get_operand_value(src)) != NULL_TERM:
            result += chr(digit)
            self.clock.tick()
            yield
        self.set_operand_value(dest, int(result))

    def i_cmp(self, var: Source, src: Source) -> Iterator:
        """
        CMP op1, op2

        Compare two operands by subtracting and set flags
        """
        op1: int = self.get_operand_value(var)
        op2: int = self.get_operand_value(src)
        self.clock.tick()
        yield
        self.alu.operation(operator.sub, op1, op2)

    def i_hlt(self) -> Iterator:
        """
        HLT

        Stop execution
        """
        self.clock.tick()
        yield
        raise ProgramExitError

    def __execute(self, instruction: Instruction) -> Iterator | None:
        result = self.get_all()[instruction.name](self, *instruction.operands)
        if isinstance(result, Iterable):
            yield from result

    def execute(self, instruction: Instruction) -> Iterator:
        """
        Execute instruction by its name
        """
        self.current = instruction
        self.current_sub = None

        if self.current.sub:
            for sub_instruction in self.current.sub:
                self.current_sub = sub_instruction
                result = self.__execute(sub_instruction)
                if isinstance(result, Iterable):
                    yield from result
        else:
            result = self.__execute(instruction)
            if isinstance(result, Iterable):
                yield from result

        # increment instruction pointer (next instruction)
        self.registers.set_instruction_pointer(self.registers.get_instruction_pointer() + 1)
        # increment ticks and instructions after execution
        self.clock.tick()
        self.clock.inst()
        yield

    @classmethod
    def get_all(cls) -> dict[str, Callable]:
        """
        Get dict with available instruction
        :return {instruction_name: instruction_executor_function}
        """
        return {key.replace("i_", ""): func for key, func in cls.__dict__.items() if key.startswith("i_")}
