# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring
from typing import Iterator

from core.machine.alu import _strip_number
from core.model import Register

__available_registers__: list[str] = ["RAX", "RBX", "RDX", "RSX", "RIP", "RSI", "RDI", "DR", "AR"]


class RegisterController:
    """
    Register Controller class
    """

    def __init__(self) -> None:
        """
        Initialize states for registers
        """
        self.__states__: dict[str, int] = {register: 0 for register in self.keys()}

    def get(self, register: Register) -> int:
        return self.__states__[register.name]

    def set(self, register: Register, value: int) -> None:
        self.__states__[register.name] = _strip_number(value)

    def get_instruction_pointer(self) -> int:
        """
        Get instruction pointer value
        """
        return self.__states__["RIP"]

    def set_instruction_pointer(self, pointer: int) -> None:
        """
        Set instruction pointer value
        """
        self.__states__["RIP"] = pointer

    def get_reg(self, r: str) -> int:
        """
        Get pointer value
        """
        return self.__states__[r]

    def set_reg(self, r: str, pointer: int) -> None:
        """
        Set pointer value
        """
        self.__states__[r] = pointer

    @staticmethod
    def contains(register_name: str) -> bool:
        """
        Check if here is register with such name
        """
        return register_name in RegisterController.keys()

    @staticmethod
    def keys() -> Iterator[str]:
        """
        Generate names of available registers
        """
        yield from __available_registers__

    def __repr__(self) -> str:
        return str(self.__states__)
