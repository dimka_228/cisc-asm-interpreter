# pylint: disable=missing-function-docstring
"""
Computer system containing:
    - clock generator
    - arithmetic logic unit
    - input-output controller
    - register controller
    - data memory controller
    - instruction controller
"""
from typing import Iterator

from core.exceptions import ProgramExitError
from core.machine.alu import ALU
from core.machine.clock import ClockGenerator, Trace
from core.machine.config import INT_ADDRESS
from core.machine.instruction_controller import InstructionController
from core.machine.int_controller import InterruptController
from core.machine.io_controller import IOController
from core.machine.memory_controller import MemoryController
from core.machine.register_controller import RegisterController
from core.model import Address, Instruction, Program, TextSection


class Computer:
    """
    Computer class
    """

    def __init__(self) -> None:
        self.clock = ClockGenerator()
        self.alu = ALU()
        self.io_controller = IOController()
        self.registers_controller = RegisterController()
        self.memory_controller = MemoryController(self.io_controller)
        self.interrupt_controller = InterruptController()
        self.instruction_executor = InstructionController(
            self.clock,
            self.alu,
            self.memory_controller,
            self.registers_controller,
            self.interrupt_controller,
        )

    def check_interrupt(self) -> bool:
        if not self.interrupt_controller.is_enabled() or len(self.io_controller.input_buffer) == 0:
            return False
        if self.interrupt_controller.is_ready():
            if self.clock.get_tick() > self.io_controller.input_buffer[0][0]:
                self.io_controller.getc()
            if self.clock.get_tick() == self.io_controller.input_buffer[0][0]:
                return True
        return False

    def do_interrupt(self) -> None:
        self.interrupt_controller.set_interrupted(True)
        self.instruction_executor.save_state()
        i: int = self.instruction_executor.get_operand_value(Address(INT_ADDRESS, ""))
        self.registers_controller.set_instruction_pointer(i)
        self.clock.tick()

    def execute_instruction(self, current_instruction: Instruction, trace: Trace):
        gen: Iterator = self.instruction_executor.execute(current_instruction)
        if trace == Trace.INST:
            for _ in gen:
                pass
            yield self
        else:
            for _ in gen:
                pass

    def execute_program(
        self, program: Program, trace: Trace, input: list[list[int]], output: list[int]
    ) -> Iterator["Computer"]:
        """
        Execute given program
        """
        self.memory_controller.load_data(program.data.memory)
        code: TextSection = program.text
        self.io_controller.input_buffer = input
        self.io_controller.output_buffer = output
        self.interrupt_controller.set_interrupted(False)
        self.interrupt_controller.set_enable(False)
        while self.registers_controller.get_instruction_pointer() < len(code.lines):
            try:
                if self.check_interrupt():
                    self.do_interrupt()

                current_instruction = code.lines[self.registers_controller.get_instruction_pointer()]
                yield from self.execute_instruction(current_instruction, trace)
            except ProgramExitError:
                return

    def __str__(self):
        lines: list[str] = [f"- INS: {self.instruction_executor.current}"]
        if self.instruction_executor.current_sub:
            lines.append(f"- SUB: {self.instruction_executor.current_sub}")
        lines.append(f"- REG: {self.registers_controller}")
        lines.append(f"- ALU: {self.alu}")
        lines.append(f"- CLK: {self.clock}")
        lines.append(f"- INT: {self.interrupt_controller}")
        return "\n".join(lines)
