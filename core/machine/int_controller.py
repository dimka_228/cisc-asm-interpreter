class InterruptController:
    """
    Interruption Controller class
    """

    def __init__(self) -> None:
        self.enable: bool = False
        self.interrupted: bool = False

    def is_interrupted(self) -> bool:
        return self.interrupted

    def set_interrupted(self, int: bool) -> None:
        self.interrupted = int

    def is_ready(self) -> bool:
        return not self.interrupted

    def is_enabled(self) -> bool:
        return self.enable

    def set_enable(self, flag: bool) -> None:
        self.enable = flag

    def __str__(self) -> str:
        return f"interrupted: {self.interrupted}, enabled: {self.enable}"
