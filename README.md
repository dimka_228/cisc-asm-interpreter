# Лабораторная работа №3. На кончиках пальцев

## Цель

- экспериментальное знакомство с устройством процессоров через моделирование; 
- получение опыта работы с компьютерной системой на нескольких уровнях организации.

### Выполнил

ФИО: Леденцов Дмитрий Андреевич<br>
Группа: P33081

### Вариант

`asm | cisc | harv | hw | instr | struct | trap | mem | pstr | prob1 | spi`

## Язык программирования

### BNF

``` ebnf
<program>       ::= <section_text> | <section_data> <section_text>

<section_data>  ::= "section .data" <line> <data>
<data>          ::= <data_line> 
                  | <data_line> <data>
<data_line>     ::= <var_name> <var_value> <line> <extra_s> 
                  | <extra_s>
<var_name>      ::= <word> ":"
<var_value>     ::= <string> | <number> | <buffer>
<string>        ::= "\'" <word> "\'" 
                  | "\"" <word> "\""
<buffer>        ::= "buf " <number>

<section_text>  ::= "section .text" <line> <instructions>
<instructions>  ::= <instruction> 
                  | <instruction> <instructions>
<instruction>   ::= <label_and_maybe_step> <line> <extra_s>
                  | <step> <line> <extra_s>
<label_and_maybe_step>  ::= <label> | <label> <step>
<label>         ::= "." <word_without_space> ":"
<step>          ::= <command> <operand(-s)>

<word>          ::= <letter_or_digit_or_space> 
                  | <letter_or_digit_or_space> <word>
<word_without_space>        ::= <letter_or_digit> 
                              | <letter_or_digit> <word_without_space>
<letter_or_digit>           ::= <letter> | <digit>
<letter_or_digit_or_space>  ::= <letter> | <digit> | <space>
<extra_s>       ::= <line_or_space> | <line_or_space> <extra_s>
<line_or_space> ::= <line> | <space>
<line>          ::= "\n" 
<spaces>        ::= <space> | <space> <spaces>
<space>         ::= " " | "\t"
<letter>        ::= [a-z] | [A-Z] | [!@#$%^&*()_+-=]
<number>        ::= <digit> | <digit> <number>
<digit>         ::= [0-9]
<comment>       ::= ";" <text>
```

Код выполняется последовательно.

Есть поддержка меток(label). Есть возможность объявить комментарий(после спец. символа ;)

Типизация статическая, слабая.

Код разбит на 2 секции:
- Секция с переменными (начинается с заголовка section .data). Здесь объявляются переменные трёх типов -- строки, числа, массивы.
- Секция кода (начинается с заголовка section .text). Здесь пишется программа с помощью меток и команд. Некоторые команды могут принимать неограниченное число аргументов. Аргументы бывают:
  - Числом (непосредственная передача).
  - Ссылкой на память (прямая адресация). Объявляется при помощи символа #. Сюда же относится обращение к каналам ввода-вывода.
  - Регистром. Обращение к регистру происходит при помощи символа %. 

## Организация памяти
Модель памяти процессора:

1. Память команд. Машинное слово -- не определено. Реализуется списком словарей, описывающих инструкции (одно слово -- одна ячейка).
2. Память данных. Машинное слово -- 32 бита, знаковое. Линейное адресное пространство. Реализуется списком чисел. 

Строки, объявленные пользователем распределяются по памяти один символ на ячейку. Вначале строки стоит ее длина(pascal style). Строка "abc" имеет вид "3abc".

``` text
     Instruction memory
+-----------------------------+
| 00  : add %rax, %rdx        |
| 01  : je end                |
|    ...                      | 
| n   : hlt                   |
|    ...                      |
+-----------------------------+

    Data memory
+-----------------------------+
| 00  : number                |
| 01  : number                |
| 03  : number                |
|    ...                      |
| 98  : number                |
| 99  : number                |
| 100 : number                |
|    ...                      |
+-----------------------------+
```


### Организация ввода-вывода.
Ввод вывод с помощью прерываний. Когда поступают входные данные, Instruction Pointer сохраняется в память вместе с флагами АЛУ, далее он перемещается на специальный адрес #INT(который пользователь устанавливает сам), 
начиная с которого последовательно выполняются все инструкции обработчика прерываний. Затем происходит
возврат из прерывания(IRET), Instruction Pointer восстанавливается в искомое положение.

Прерывания запрещаются автоматически во время выполнения прерывания. По умолчанию прерывания запрещены, их можно включить и отключить вручную командами EI, DI.

### Структура программы

``` asm
; секция данных
section .data
    var: (STR) | (NUM) | (buf NUM)

; секция кода
section .text
    [label:] instr [op1, [op2, [...]]]
```

### Секция данных

``` asm
section .data
    HELLO:      "Hello"         ; строка
    NUMBER_HEX: 0xDEAD          ; число в 16 СС
    NUMBER_OCT: 0o1337          ; число в 8 СС
    NUMBER_BIN: 0b10110         ; число в 2 СС
    NUMBER_DEC: -81             ; число в 10 СС
    ARRAY:      buf 20          ; массив из 20 элементов
    NULL_TERM:  0x00
```

### Секция кода

``` asm
section .text
    .print_char:                        ; метка
        MOV %rdx, #ARRAY[%rdi]          ; загрузить
                                        ; в регистр RDX
                                        ; значение ARRAY[RDI]
                                        ; (косвенная адресация) 
        
        CMP %rdx, #LEN                  ; сравнить
                                        ; регистр RDX
                                        ; и переменную
                                        ; (прямая адресация)

        JE .exit                        ; если равны
                                        ; прыгаем на .exit

        MOV #STDOUT, %rdx               ; выводим символ
                                        ; из регистра RDX

        INC %rdi                        ; инкрементируем
                                        ; регистр RDI
        
        JMP .print_char                 ; прыгаем на .print_char
    
    .exit:
        HLT                             ; завершение программы
```

### Система команд

Некоторые инструкции могут принимать неограниченное количество операндов. В таком случае при трансляции сложная инструкция преобразуется в несколько простых.

``` asm
MUL %RAX, #VAR              ; %RAX * #VAR        -> %RAX
MUL %RAX, #VAR, 0xf1        ; #VAR * 0xf1        -> %RAX
MUL %RAX, #VAR, 0xf1, %RDX  ; #VAR * 0xf1 * %RDX -> %RAX
```

### Набор инструкции

| Syntax | Mnemonic   | Comment                                           |
|:-------|:-----------|:--------------------------------------------------|
| `ADD`  | summary    | Сложение операндов и сохранение                   |
| `SUB`  | subtract   | Вычитание операндов и сохранение                  |
| `MUL`  | multiply   | Умножение операндов и сохранение                  |
| `DIV`  | divide     | Деление операндов и сохранение                    |
| `MOD`  | mod_div    | Получение остатка от деления и сохранение         |
| `XOR`  | xor        | Логическое исключающее "ИЛИ" и сохранение         |
| `AND`  | and        | Логическое "И" и сохранение                       |
| `OR`   | or         | Логическое "ИЛИ" и сохранение                     |
| `DEC`  | decrement  | Декремент операнда                                |
| `INC`  | increment  | Инкремент операнда                                |
| `IRET` | iret       | Возврат из прерывания                             |
| `DI`   | di         | Запрет прерываний                                 |
| `EI`   | ei         | Разрешение прерываний                             |
| `JMP`  | jmp        | Безусловный переход к метке                       |
| `JE`   | je         | Переход, если равенство (Z=1)                     |
| `JNE`  | jne        | Переход, если не равенство (Z=0)                  |
| `JL`   | jl         | Переход, если меньше (N=1)                        |
| `JG`   | jg         | Переход, если больше (N=0, Z=0)                   |
| `JLE`  | jle        | Переход, если меньше или равно (N=1 or Z=1)       |
| `JGE`  | jge        | Переход, если больше или равно (N=0)              |
| `MOV`  | move       | Передать значение из операнда в операнд           |
| `MOVN` | move_to_io | Передать значение в канал вывода                  |
| `LDN`  | load_input | Сохранить значение из канала ввода                |
| `CMP`  | compare    | Установка флагов по операции вычитания операндов  |
| `HLT`  | halt       | Остановка программы                               |

Подробное описание команд можно найти [тут](resources/instructions.md)

## Транслятор

Имеет две стадии:

- Предобработка:
    - Удаление лишних символов (пробелов, табуляций)
    - Удаление комментариев
- Трансляция:
  - Текст -> секции
  - Секция -> строчки
  - Строчка -> метка, инструкция
  - Инструкция -> команда, операнды
  - Операнд -> константа, регистр, адрес, метка

Пример программы, прошедшей трансляцию (сериализация через pickle):

``` python
Program(data=DataSection(var_to_addr={'FLAG': 4,
                                      'INT': 3,
                                      'REG': 5,
                                      'STDIN': 0,
                                      'STDOUT': 1},
                         memory=[0, 0, 0, 0, 0]),
        text=TextSection(labels={'.exit': 2, '.read_char': 3},
                         lines=[Instruction(name='mov',
                                            operands=[Address(value=3,
                                                              label='INT'),
                                                      Label(name='.read_char',
                                                            value=3)],
                                            sub=[]),
                                Instruction(name='ei', operands=[], sub=[]),
                                Instruction(name='hlt', operands=[], sub=[]),
                                Instruction(name='mov',
                                            operands=[Register(name='RSX'),
                                                      Address(value=0,
                                                              label='STDIN')],
                                            sub=[]),
                                Instruction(name='mov',
                                            operands=[Address(value=1,
                                                              label='STDOUT'),
                                                      Register(name='RSX')],
                                            sub=[]),
                                Instruction(name='iret', operands=[], sub=[])]))


```

## Модель процессора
![plot](resources/model.jpg)


Все составляющие реализованы в модуле `core.machine`.

[Компьютер](core/machine/computer.py) состоит из следующих частей:

### ALU

[core/machine/alu.py](/core/machine/alu.py)

Арифметико-логическое устройство. Все операции происходят через него, после каждой операции ставятся флаги:

- N (negative | отрицательное число, единица в старшем бите)
- Z (zero | нуль)
- V (overflow | переполнение)
- C (carry | перенос единицы в бит, следующим за старшим)

### Тактовый генератор

[core/machine/clock.py](core/machine/clock.py)

Генерирует такты!

Содержит счетчик тактов и инструкций.

### Контроллер регистров

[core/machine/register_controller.py](core/machine/register_controller.py)

Отвечает за работу с регистрами. Регистры делятся на три группы:

- Регистры данных: RAX, RBX, RDX, RSX (хранят данные)
- Регистры указателей: RIP (указатель на инструкцию)
- Регистры индексов: RSI, RDI (хранят адреса)


### Контроллер памяти данных

[core/machine/memory_controller.py](core/machine/memory_controller.py)

Отвечает за работу с ячейками памяти данных.

При инициализации создается массив размера `MEMORY_SIZE`.

Загрузка данных происходит с помощью метода `load_data`.

Имеются геттеры и сеттеры, содержащие проверки на корректность адреса и значения.

### Контроллер инструкций

[core/machine/instruction_controller.py](core/machine/instruction_controller.py)

Основная логика реализована здесь. Инструкции принимают операнды следующих типов:

```python
Destination: TypeAlias = Address | IndirectAddress | Register
Source: TypeAlias = Address | IndirectAddress | Register | Constant
```

Destination - это приёмник, куда мы записываем значение.  Source - это "исходник", откуда мы берем значения для операции. Константа не может быть приёмником.

При записи или чтении значений из операндов они транслируются в обычные числа.

Работа с неограниченным количеством операндов происходит с помощью свёртки, при этом отдельно учитывается случай с двумя операндами.

2 операнда -> применяется операция между приёмником и исходником, результат записывается в приёмник.

```
ADD x1, x2              ; (x1 + x2) -> x1
```

3+ операнда -> применяется операция между исходниками слева направо, результат записывается в приёмник.

```
ADD x1, x2, x3, x4, x5  ; (((x2 + x3) + x4) + x5) -> x1
```

После трансляции программа преобразуется в list[Instruction]

### Контроллер ввода-вывода

[core/machine/io_controller.py](core/machine/io_controller.py)

Отвечает за ввод и вывод символа в стандартные потоки.

Взаимодействие происходит через ячейки памяти с адресами 0, 1.


## Тестирование

В качестве тестов для machine и translator происходит запуск 4 программ: 

1. [cat](test/examples/cat.pyasm)
2. [hello](test/examples/hello.pyasm)
3. [hello_user_name](test/examples/hello_user_name.pyasm)
4. [prob1](test/examples/prob1.pyasm)

Golden тесты можно найти [тут](golden_test.py)

Журналы и коды алгоритмов можно посмотреть в каталоге [test/examples](test/examples).

| ФИО          | алг.            | LoC | code байт | code инстр. | инстр. | такт. | 
|--------------|-----------------|-----|-----------|-------------|--------|-------|
| Леденцов ДА. | cat             | 11  | -         | 6           | 28     | 46    |
| Леденцов ДА. | hello           | 18  | -         | 10          | 69     | 116   |
| Леденцов ДА. | hello_user_name | 53  | -         | 27          | 248    | 430   |
| Леденцов ДА. | prob1           | 27  | -         | 25          | 8333   | 16277 |


CI:

заготовленный gitlab.yml для питона
```yml
lab3:
  stage: test
  image:
    name: ryukzak/python-tools
    entrypoint: [ "" ]
  script:
    - poetry install
    - coverage run -m pytest --verbose
    - find . -type f -name "*.py" | xargs -t coverage report
    - ruff format --check .
    - ruff check .
```














