# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring
import contextlib
import glob
import os
import sys
from types import UnionType
from typing import Annotated, Optional, Type

import typer
import ruamel.yaml
from ruamel.yaml.scalarstring import PreservedScalarString
from core.exceptions import CatchPyAsmException, PyAsmError
from core.file_helper import read_program_from_file, translate_asm_file, translate_bin_to_readable_format
from core.machine import Computer, InstructionController, Trace
from core.model import Program
from core.translator.util import parse_input, split_output_log

app = typer.Typer(help="PyAsm Runner")
MAX_YAML_LOG_SIZE: int = 100000


def print_exception(error: PyAsmError) -> None:
    """
    Print PyAsmException message in stderr
    """
    exc_type: Type[PyAsmError] = type(error)
    typer.echo(typer.style(f"{exc_type.__name__}: {error!s}", fg=typer.colors.RED), err=True)


@app.command(name="generate-instruction-docs")
def generate_instruction_docs(
    doc_file_name: Optional[str] = typer.Option("./resources/instructions.md", "--output", "-o"),
) -> None:
    """
    Generate documentation for pyasm instructions
    """
    doc_instructions: list[str] = ["### Instructions\n"]
    for name, function in InstructionController.get_all().items():
        doc_instructions.append(f"#### {name}\n")
        docstring = function.__doc__
        if docstring:
            docstring = docstring.strip()
        doc_instructions.append("```\n" f"{docstring}\n" "```\n")
        for key, value in function.__annotations__.items():
            if isinstance(value, type):
                value = value.__name__
            elif isinstance(value, UnionType):
                value = " | ".join(x.__name__ for x in value.__args__)
            doc_instructions.append(f"- **{key}**: `{value}`\n")

    with open(doc_file_name, "w", encoding="utf8") as file:
        file.writelines(doc_instructions)


@app.command(name="translate")
def translate(
    asm_file_name: str,
    object_file_name: Optional[str] = typer.Option(None, "--output", "-o"),
    debug_output: Annotated[bool, typer.Option("--debug/--no-debug", "-d/-D")] = False,
) -> None:
    """
    Translate .asm code to object file
    """
    if object_file_name is None:
        object_file_name = f"{asm_file_name}.o"

    with CatchPyAsmException() as catcher:
        translate_asm_file(asm_file_name, object_file_name)
        if debug_output:
            translate_bin_to_readable_format(object_file_name, f"{asm_file_name}.prog")
    if catcher.exception:
        print_exception(catcher.exception)
        sys.exit(1)


@app.command(name="exec")
def execute(
    obj_file_name: str,
    input: Optional[str] = typer.Option("", "--input", "-i"),
    trace: Trace = typer.Option(Trace.NO, "--trace", "-t", case_sensitive=False),
) -> None:
    """
    Execute object file
    """
    program: Program = read_program_from_file(obj_file_name)
    computer: Computer = Computer()
    with CatchPyAsmException() as catcher:
        output: list[int] = []
        for ex in computer.execute_program(program, trace, parse_input(input), output):
            if not trace:
                continue
            print(ex, end="\n\n")
        print("".join(list(map(lambda char: chr(char), output))), end="")
    if catcher.exception:
        print_exception(catcher.exception)
        sys.exit(1)


@app.command(name="run")
def run(
    asm_file_name: str,
    object_file_name: Optional[str] = typer.Option(None, "--output", "-o"),
    input: Optional[str] = typer.Option("", "--input", "-i"),
    trace: Trace = typer.Option(Trace.NO, "--trace", "-t", case_sensitive=False),
) -> None:
    """
    Translate and execute .pyasm file
    """
    if object_file_name is None:
        object_file_name = f"{asm_file_name}.o"

    translate(asm_file_name, object_file_name)
    execute(object_file_name, input, trace)


@app.command(name="compile-and-run-all")
def compile_and_run_all(
    dir_name: Optional[str] = typer.Option("./golden", "--dir", "-d"),
) -> None:
    """
    Run all files in directory, save logs and generate journal tests
    """
    yaml = ruamel.yaml.YAML()
    for file_path in glob.glob(dir_name + "/*.yml"):
        file_name: str = os.path.basename(file_path)
        print(file_name)

        with open(file_path) as file:
            test_data = yaml.load(file)
            test_input: str = test_data.get("input")
            test_source: str = test_data.get("source")

        translate(test_source, f"{test_source}.o", True)
        with open(f"{test_source}.log", "w", encoding="utf-8") as log_file:
            with contextlib.redirect_stdout(log_file):
                execute(f"{test_source}.o", test_input, Trace.INST)

        with open(f"{test_source}.log", encoding="utf-8") as log_file, open(file_path, "w") as file:
            test_data["log"] = PreservedScalarString(split_output_log(log_file.read())[0][0:MAX_YAML_LOG_SIZE])
            yaml.dump(test_data, file)


if __name__ == "__main__":
    app()
