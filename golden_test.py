# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring
import io
import os
import tempfile
from core.machine import Trace
from unittest.mock import patch
from core.translator.util import regularize_string, split_output_log

import main
import pytest


@pytest.mark.golden_test("golden/*.yml")
def test(golden):
    with tempfile.TemporaryDirectory() as tmpdirname:
        source = os.path.join(tmpdirname, "source.asm")
        binary = os.path.join(tmpdirname, "source.o")
        with open(source, "w", encoding="utf-8") as src, open(golden["source"], encoding="utf-8") as text:
            src.write(text.read())

        with patch("sys.stdout", new_callable=io.StringIO) as output:
            main.run(source, binary, golden["input"], Trace.INST)
        arr: list[str] = split_output_log(output.getvalue())
        log: str = arr[0]
        out: str = arr[1]
        assert out == regularize_string(golden.out["output"])
        assert log.startswith(golden.out["log"])
